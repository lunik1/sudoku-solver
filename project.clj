(defproject sudoku-solver "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :license {:name "BSD 3 Clause"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :plugins [[lein-cljfmt "0.5.6"]]
  :main ^:skip-aot sudoku-solver.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
