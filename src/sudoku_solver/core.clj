(ns sudoku-solver.core
  (:gen-class)
  (:require [clojure.set]))

(def allowed-values
  "The allowed values in a sudoku"
  #{nil :1 :2 :3 :4 :5 :6 :7 :8 :9})

(defn -main
  [& args])

(defn square-index
  "Returns the lowest index (top left) in the nth sudoku square"
  [n]
  (+ (* (quot n 3) 27) (* (mod n 3) 3)))

(defn distinct-or-nil?
  "Returns true if no two arguments are =, but allowing duplicate nils"
  [& more]
  (or
   (every? nil? more)
   (apply distinct? (remove nil? more))))

(defn which-row
  "Returns the sudoku row which index x lies in"
  [x]
  (quot x 9))

(defn which-column
  "Returns the sudoku column which index x lies in"
  [x]
  (mod x 9))

(defn which-square
  "Returns the sudoku square which index x lies in"
  [x]
  (+ (* (quot x 27) 3) (quot (which-column x) 3)))

(defn get-row
  "Returns row n from sudoku su"
  [su n]
  (subvec su (* n 9) (* (inc n) 9)))

(defn get-column
  "Returns column n from sudoku su"
  [su n]
  (vec (take-nth 9 (drop n su))))

(defn get-square
  "Returns square n from sudoku su"
  [su n]
  (let [x (square-index n)]
    (vec (mapcat #(subvec su % (+ % 3)) (range x (+ x 19) 9)))))

(defn row-valid?
  "Returns distinct-or-nil? for row n from sudoku su"
  [su n]
  (apply distinct-or-nil? (get-row su n)))

(defn column-valid?
  "Returns distinct-or-nil? for column n from sudoku su"
  [su n]
  (apply distinct-or-nil? (get-column su n)))

(defn square-valid?
  "Returns distinct-or-nil? for square n from sudoku su"
  [su n]
  (apply distinct-or-nil? (get-square su n)))

(defn sudoku-valid?
  "Returns true if sudoku su is valid, false otherwise.

  A sudoku is valid if it contains only the symbols :1, :2, :3, :4, :5, :6, :7,
  :8, :9, or nil (representing an empty space). Furthermore, every row, column,
  and square may contain each keyword symbol only once."
  [su]
  (and
   (clojure.set/subset? (set su) allowed-values)
   (not-any? false? (mapcat (juxt
                             (partial row-valid? su)
                             (partial column-valid? su)
                             (partial square-valid? su)) (range 9)))))
