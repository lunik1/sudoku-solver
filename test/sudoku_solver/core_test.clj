(ns sudoku-solver.core-test
  (:require [clojure.test :refer :all]
            [sudoku-solver.core :refer :all]))

(def su1 [ :0  :1  :2  :3  :4  :5  :6  :7  :8
           :9 :10 :11 :12 :13 :14 :15 :16 :17
          :18 :19 :20 :21 :22 :23 :24 :25 :26
          :27 :28 :29 :30 :31 :32 :33 :34 :35
          :36 :37 :38 :39 :40 :41 :42 :43 :44
          :45 :46 :47 :48 :49 :50 :51 :52 :53
          :54 :55 :56 :57 :58 :59 :60 :61 :62
          :63 :64 :65 :66 :67 :68 :69 :70 :71
          :72 :73 :74 :75 :76 :77 :78 :79 :80])

(def su2 [nil nil  :2 nil  :3 nil nil nil  :8
          nil nil nil nil nil  :8 nil nil nil
          nil  :3  :1 nil  :2 nil nil nil nil
          nil  :6 nil nil  :5 nil  :2  :7 nil
          nil  :1 nil nil nil nil nil  :5 nil
           :2 nil  :4 nil  :6 nil nil  :3  :1
          nil nil nil nil  :8 nil  :6 nil  :5
          nil nil nil nil nil nil nil  :1  :3
          nil nil  :5  :3  :1 nil  :4 nil nil])

(def su3 [nil nil  :2 nil  :3 nil nil nil  :8
          nil nil nil nil nil  :8 nil nil nil
          nil  :3  :1 nil  :2 nil nil nil nil
          nil  :6 nil nil  :5 nil  :2  :7 nil
          nil  :1 nil nil nil nil nil  :5 nil
          :2 nil  :4 nil  :6 nil nil  :3  :1
          nil nil nil nil  :8 nil  :6 nil  :5
          nil nil nil nil nil nil  :4  :4  :3
          nil nil  :5  :3  :1 nil  :4 nil nil])

(def su4 (vec (take 81 (repeat nil))))

(deftest square-index-test
  (is (= '(0 3 6 27 30 33 54 57 60) (map square-index (range 9)))))

(deftest distinct-or-nil?-test
  (is (true? (distinct-or-nil? 0 1 2 3)))
  (is (true? (distinct-or-nil? nil nil 0 1 2 3 nil)))
  (is (true? (distinct-or-nil? 0)))
  (is (false? (distinct-or-nil? nil nil 0 0 1 2 nil))))

(deftest which-row-test
  (is (= (range 9) (map which-row (range 0 90 10)))))

(deftest which-column-test
  (is (= (range 9) (map which-column (range 0 90 10)))))

(deftest which-square-test
  (is (= [0 0 0 4 4 4 8 8 8] (map which-square (range 0 90 10)))))

(deftest get-row-test
  (is (= su1 (mapcat (partial get-row su1) (range 9)))))

(deftest get-column-test
  (is (= su1 (apply interleave (map (partial get-column su1) (range 9))))))

(deftest get-square-test
  (is (= (get-square su1 0) [:0  :1  :2  :9 :10 :11 :18 :19 :20]))
  (is (= (get-square su1 4) [:30 :31 :32 :39 :40 :41 :48 :49 :50]))
  (is (= (get-square su1 8) [:60 :61 :62 :69 :70 :71 :78 :79 :80])))

(deftest row-valid?-test
  (is (every? (partial row-valid? su2) (range 9)))
  (is (false? (row-valid? su3 7)))
  (is (every? (partial row-valid? su4) (range 9))))

(deftest column-valid?-test
  (is (every? (partial column-valid? su2) (range 9)))
  (is (false? (column-valid? su3 6)))
  (is (every? (partial column-valid? su4) (range 9))))

(deftest square-valid?-test
  (is (every? (partial square-valid? su2) (range 9)))
  (is (false? (square-valid? su3 8)))
  (is (every? (partial square-valid? su4) (range 9))))

(deftest sudoku-valid?-test
  (is (false? (sudoku-valid? su1)))
  (is (true? (sudoku-valid? su2)))
  (is (false? (sudoku-valid? su3)))
  (is (true? (sudoku-valid? su4))))
